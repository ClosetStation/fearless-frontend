window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference');

    const form = document.getElementById('create-attendee-form')
    form.addEventListener('submit', async event => {
        event.preventDefault()
        const formData = new FormData(form)
        const json = JSON.stringify(Object.fromEntries(formData.entries()))

        const attendeeUrl = 'http://localhost:8001/api/attendees/'; //url changed
        const fetchOptions = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
        },
        }
        const attendeeResponse = await fetch(attendeeUrl, fetchOptions);
        if (attendeeResponse.ok) {
        const success = document.getElementById('success-message');
        form.classList.add('d-none');
        success.classList.remove('d-none');
        } else {
        console.log(attendeeResponse);
        }
    });
    const url = 'http://localhost:8000/api/conferences/';

    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();

      for (let conference of data.conferences) {
        const option = document.createElement('option');
        option.value = conference.href;
        option.innerHTML = conference.name;
        selectTag.appendChild(option);
      }
      //Here, add the "d-none" class to the loading icon
        let spinner = document.querySelector('#loading-conference-spinner')
        spinner.classList.add('d-none')

    //   Here, remove the "dd-none" class fromt he select tag
        let selector = document.querySelector('#conference')
        selector.classList.remove('d-none')

        // Another way to do this
    //     selectTag.classList.remove('d-none');
    //     const spinner = document.getElementById('loading-conference-spinner');
    //     spinner.classList.add('d-none');
    // }
    // Another way to do this

    }})
